import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {RepositoryMixin, SchemaMigrationOptions} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import fs from 'fs';
import path from 'path';
import {OrderRepository} from './repositories/order.repository';
import {ProductRepository} from './repositories/product.repository';
import {PromotionRepository} from './repositories/promotion.repository';
import {MySequence} from './sequence';
import YAML = require('yaml');

export {ApplicationConfig};

export class MsApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }

  async migrateSchema(options?: SchemaMigrationOptions): Promise<void> {
    await super.migrateSchema(options);

    console.log('START MIGRATION');
    const productRepo = await this.getRepository(ProductRepository);
    await productRepo.deleteAll();
    const productDir = path.join(__dirname, '../fixtures/products');
    const productFiles = fs.readdirSync(productDir);
    for (const file of productFiles) {
      if (file.endsWith('.yml')) {
        const productFile = path.join(productDir, file);
        const yamlString = fs.readFileSync(productFile, 'utf8');
        const product = YAML.parse(yamlString);
        try {
          await productRepo.create(product);
          console.log('product created successfully');
        } catch (error) {
          console.log('create product error:', error);
        }
      }
    }

    const promotionRepo = await this.getRepository(PromotionRepository);
    await promotionRepo.deleteAll();
    const promotionDir = path.join(__dirname, '../fixtures/promotions');
    const promitionFiles = fs.readdirSync(promotionDir);
    for (const file of promitionFiles) {
      if (file.endsWith('.yml')) {
        const promotionFile = path.join(promotionDir, file);
        const yamlString = fs.readFileSync(promotionFile, 'utf8');
        const promotion = YAML.parse(yamlString);
        try {
          await promotionRepo.create(promotion);
          console.log('promotion created successfully');
        } catch (error) {
          console.log('create promotion error:', error);
        }
      }
    }

    const orderRepo = await this.getRepository(OrderRepository);
    await orderRepo.deleteAll();

    console.log('END MIGRATION');
  }

  // Unfortunately, TypeScript does not allow overriding methods inherited
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  async start() {
    await this.migrateSchema();
    return super.start();
  }
}
