import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    hiddenProperties: ['productIds', 'startDate', 'endDate'],
  },
})
export class Promotion extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'array',
    itemType: 'string',
    required: true,
  })
  productIds: string[];

  @property({
    type: 'number',
    required: true,
  })
  percent: number;

  @property({
    type: 'number',
    required: true,
  })
  startDate: number;

  @property({
    type: 'number',
    required: true,
  })
  endDate: number;

  constructor(data?: Partial<Promotion>) {
    super(data);
  }
}

export interface PromotionRelations {
  // describe navigational properties here
}

export type PromotionWithRelations = Promotion & PromotionRelations;
