import {repository} from '@loopback/repository';
import {post, requestBody} from '@loopback/rest';
import {
  OrderRepository,
  ProductRepository,
  PromotionRepository,
} from '../repositories';

export class OrderController {
  constructor(
    @repository(ProductRepository)
    public productRepository: ProductRepository,
    @repository(PromotionRepository)
    public promotionRepository: PromotionRepository,
    @repository(OrderRepository)
    public orderRepository: OrderRepository,
  ) {}

  @post('/order')
  async order(
    @requestBody()
    order: {
      productId: string;
      userId: string;
      quantity: number;
      promotionId?: string;
    },
  ) {
    try {
      const product = await this.productRepository.findById(order.productId);
      const orderedes = await this.orderRepository.find({
        where: {
          promotionId: order.promotionId ?? new Date().toString(),
          userId: order.userId,
          productType: product.type,
        },
      });
      if (orderedes.length) {
        return {
          message:
            'The promotion just adopt for each user per one product type only',
          success: false,
        };
      }
      const newOrder = {
        productId: order.productId,
        productType: product.type,
        promotionId: order.promotionId,
        userId: order.userId,
        quantity: order.quantity,
        createdAt: new Date().getTime(),
      };
      if (product.quantity - order.quantity >= 0) {
        await this.productRepository.updateById(product.id ?? '', {
          quantity: product.quantity - order.quantity,
        });
        const created = await this.orderRepository.create(newOrder);
        return {
          message: 'Order placed successfully',
          data: created.id,
          success: true,
        };
      } else {
        const offset = product.stock - product.remain;
        if (order.quantity > offset) {
          return {message: 'Product no longer available', success: false};
        } else {
          const offsetRemain = order.quantity - product.quantity;
          await this.productRepository.updateById(product.id ?? '', {
            remain: product.remain + offsetRemain,
            quantity: 0,
          });
          const created = await this.orderRepository.create(newOrder);
          return {
            message: 'Order placed successfully',
            data: created.id,
            success: true,
          };
        }
      }
    } catch (error) {
      return error;
    }
  }
}
