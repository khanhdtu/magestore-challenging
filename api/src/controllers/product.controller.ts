import {repository} from '@loopback/repository';
import {get} from '@loopback/rest';
import {ProductRepository, PromotionRepository} from '../repositories';

export class ProductController {
  constructor(
    @repository(ProductRepository)
    public productRepository: ProductRepository,
    @repository(PromotionRepository)
    public promotionRepository: PromotionRepository,
  ) {}

  @get('/products')
  async getProducts() {
    try {
      const products = await this.productRepository.find();
      const promotion = await this.promotionRepository.findOne({
        where: {
          endDate: {
            gte: new Date().getTime(),
          },
        },
      });
      products.map(p => {
        if (promotion?.productIds.includes(p.id ?? '')) {
          p.promotion = promotion;
        }
      });
      return products;
    } catch (error) {
      return error;
    }
  }
}
