import { Component, Input, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Product } from '../models/product.model';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent {
  @Input('product') product: Product;
  quantity: number = 1;
  success: boolean;
  message: string;
  orderNumber: string;
  constructor(public service: AppService) {}

  async onConfirm() {
    const res = await this.service.order(this.product.id, this.product?.promotion?.id ?? null, this.quantity)
    this.success = res.success
    this.message = res.message
    this.orderNumber = res.data
  }

  onCancel() {
    this.service.setCurrentIndex(-1)
    this.success = null
    this.message = ''
    this.orderNumber = ''
    this.quantity = 1
  }
}
