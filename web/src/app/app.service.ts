import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Product } from './models/product.model';
import { User } from './models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  url = 'http://localhost:3000'
  products = new BehaviorSubject<Product[]>([])
  currentIndex = new BehaviorSubject<number>(-1)
  user: User;

  constructor(public http: HttpClient) {
    this.http.get<Product[]>(`${this.url}/products`)
    .toPromise()
    .then(products => this.products.next(products))
    .catch(console.error)
  }

  setCurrentIndex(index: number) {
    this.currentIndex.next(index)
  }

  async order(productId, promotionId, quantity) {
    return this.http.post<{message: string, success: boolean, data?: string}>(`${this.url}/order`, {
      userId: this.user.providerId,
      productId,
      promotionId,
      quantity,
    }).toPromise()
  }
}
