export interface User {
    providerId: string
    provider: string
    picture?: string
}