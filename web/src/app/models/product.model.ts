export interface Product {
    id: string
    name: string
    thumb: string
    type: string
    price: number
    promotion?: {
        id: string
        name: string
        percent: number
    }
}