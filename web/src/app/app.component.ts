import { Component } from '@angular/core';
import { FacebookService, } from 'ngx-facebook';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'web';
  currentProductIndex: number;
  constructor(public service: AppService, public fb: FacebookService) {
    fb.init({
      appId: '560844508251237',
      xfbml: true,
      version: 'v2.8',
    })
    service.currentIndex.asObservable()
    .subscribe(index => {
      this.currentProductIndex = index
      console.log(index)
    })
  }

  async onBuy(index) {
    if (!this.service.user) {
      const res = await this.fb.login()
      this.service.user = {
        providerId: res.authResponse.userID,
        provider: (res.authResponse as any).graphDomain
      }
      this.service.setCurrentIndex(index);
    } else {
      this.service.setCurrentIndex(index);
    }
  }
}
